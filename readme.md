# Introduction

Common libraries for applications related to the [Lions Multiple District 410 2021 Convention](https://www.lionsconvention2021.co.za/).

# Associated Applications

See [this Gitlab group](https://gitlab.com/md410_2021_conv) for associated applications.
